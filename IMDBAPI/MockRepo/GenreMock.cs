﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using Moq;

namespace IMDBAPI.MockRepo
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();
        public static int _id = 7;

        public static void MockGetAll()
        {
            List<Genre> entityList = new()
            {
                new Genre()
                {
                    Id=1,
                    Name="Action",

                },
                new Genre()
                {
                    Id=2,
                    Name="War"
                },
                new Genre()
                {
                    Id=3,
                    Name="Superhero"
                },
                new Genre()
                {
                    Id=4,
                    Name="Science Fiction"
                },
                new Genre()
                {
                    Id=5,
                    Name="Sports"
                },
                new Genre()
                {
                    Id=6,
                    Name="Drama"
                }
            };
            genreRepoMock.Setup(repo => repo.GetAll()).Returns(() =>
            {
                return entityList;
            });
        }

        public static void MockGet()
        {
            List<Genre> entityList = new()
            {
                new Genre()
                {
                    Id=1,
                    Name="Action",

                },
                new Genre()
                {
                    Id=2,
                    Name="War"
                },
                new Genre()
                {
                    Id=3,
                    Name="Superhero"
                },
                new Genre()
                {
                    Id=4,
                    Name="Science Fiction"
                },
                new Genre()
                {
                    Id=5,
                    Name="Sports"
                },
                new Genre()
                {
                    Id=6,
                    Name="Drama"
                }
            };

            genreRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                List<Genre> result = new List<Genre>();
                result.Add(entityList.Find(entity => entity.Id == id));
                return result;
            });
        }

        public static void MockPost()
        {
            List<Genre> entityList = new()
            {
                new Genre()
                {
                    Id=1,
                    Name="Action",

                },
                new Genre()
                {
                    Id=2,
                    Name="War"
                },
                new Genre()
                {
                    Id=3,
                    Name="Superhero"
                },
                new Genre()
                {
                    Id=4,
                    Name="Science Fiction"
                },
                new Genre()
                {
                    Id=5,
                    Name="Sports"
                },
                new Genre()
                {
                    Id=6,
                    Name="Drama"
                }
            };

            genreRepoMock.Setup(repo => repo.Post(It.IsAny<string>())).Callback((string name) =>
            {
                entityList.Add(new Genre()
                {
                    Id = _id,
                    Name = name,
                    
                });
                _id++;
            });

        }

        public static void MockPut()
        {

            List<Genre> entityList = new List<Genre>()
            {
                new Genre()
                {
                    Id=1,
                    Name="Action",

                },
                new Genre()
                {
                    Id=2,
                    Name="War"
                },
                new Genre()
                {
                    Id=3,
                    Name="Superhero"
                },
                new Genre()
                {
                    Id=4,
                    Name="Science Fiction"
                },
                new Genre()
                {
                    Id=5,
                    Name="Sports"
                },
                new Genre()
                {
                    Id=6,
                    Name="Drama"
                }
            };

            genreRepoMock.Setup(repo => repo.Put(It.IsAny<int>(), It.IsAny<string>())).Callback((int id, string name) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
                entityList.Add(new Genre()
                {
                    Id = id,
                    Name = name,
                });
            });
        }

        public static void MockDelete()
        {

            List<Genre> entityList = new()
            {
                new Genre()
                {
                    Id=1,
                    Name="Action",

                },
                new Genre()
                {
                    Id=2,
                    Name="War"
                },
                new Genre()
                {
                    Id=3,
                    Name="Superhero"
                },
                new Genre()
                {
                    Id=4,
                    Name="Science Fiction"
                },
                new Genre()
                {
                    Id=5,
                    Name="Sports"
                },
                new Genre()
                {
                    Id=6,
                    Name="Drama"
                }
            };

            genreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>())).Callback((int id) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
            });

        }

        public static void MockGetByMovieId()
        {
            List<GenreResponse> entityList = new()
            {
                new GenreResponse()
                {
                    Id = 1,
                    Name = "Action",

                },
                new GenreResponse()
                {
                    Id = 2,
                    Name = "War"
                },
                new GenreResponse()
                {
                    Id = 3,
                    Name = "Superhero"
                },
                new GenreResponse()
                {
                    Id = 4,
                    Name = "Science Fiction"
                },
                new GenreResponse()
                {
                    Id = 5,
                    Name = "Sports"
                },
                new GenreResponse()
                {
                    Id = 6,
                    Name = "Drama"
                }
            };
            genreRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
            {
                List<GenreResponse> result = new();
                if (movieId == 1)
                {
                    var genreIdList = new List<int>() { 5, 6 };
                    foreach (var genreId in genreIdList){
                        result.Add((entityList.Find(entity => entity.Id == genreId)));
                    }
                }

                if (movieId == 2)
                {
                    var genreIdList = new List<int>() { 1, 2, 3, 4 };
                    foreach (var genreId in genreIdList)
                    {
                        result.Add((entityList.Find(entity => entity.Id == genreId)));
                    }
                }


                return result;

            });


        }

    }

    
}
