﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using Moq;

namespace IMDBAPI.MockRepo
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();
        public static int _id = 4; //3 actors previously available


        public static void MockGetAll()
        {
            List<Producer> enityList = new List<Producer>()
            {
                new Producer()
                {
                    Id=1,
                    Name="Kevin Feigi",
                    Gender="Male",
                    DOB= new DateTime(1973,06,02),
                    Bio="British"
                },
                new Producer()
                {
                    Id=2,
                    Name="Robert N. Fried",
                    Gender="Male",
                    DOB=new DateTime(1963,01,16),
                    Bio="Ukranian"

                },
                new Producer()
                {
                    Id=3,
                    Name="Kathryn Bigelow",
                    Gender="Female",
                    DOB=new DateTime(1951,11,27),
                    Bio="Ukranian"
                }
            };
            producerRepoMock.Setup(repo => repo.GetAll()).Returns(() =>
            {
                return enityList;
            });
        }

        public static void MockGet()
        {
            List<Producer> entityList = new List<Producer>()
            {
                new Producer()
                {
                    Id=1,
                    Name="Kevin Feigi",
                    Gender="Male",
                    DOB= new DateTime(1973,06,02),
                    Bio="British"
                },
                new Producer()
                {
                    Id=2,
                    Name="Robert N. Fried",
                    Gender="Male",
                    DOB=new DateTime(1963,01,16),
                    Bio="Ukranian"

                },
                new Producer()
                {
                    Id=3,
                    Name="Kathryn Bigelow",
                    Gender="Female",
                    DOB=new DateTime(1951,11,27),
                    Bio="Ukranian"
                }
            };
            
            producerRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                List<Producer> result = new List<Producer>();
                result.Add(entityList.Find(entity => entity.Id == id));
                return result;
            });
        }

        public static void MockPost()
        {
            List<Producer> entityList = new List<Producer>()
            {
                new Producer()
                {
                    Id=1,
                    Name="Kevin Feigi",
                    Gender="Male",
                    DOB= new DateTime(1973,06,02),
                    Bio="British"
                },
                new Producer()
                {
                    Id=2,
                    Name="Robert N. Fried",
                    Gender="Male",
                    DOB=new DateTime(1963,01,16),
                    Bio="Ukranian"

                },
                new Producer()
                {
                    Id=3,
                    Name="Kathryn Bigelow",
                    Gender="Female",
                    DOB=new DateTime(1951,11,27),
                    Bio="Ukranian"
                }
            };

            producerRepoMock.Setup(repo => repo.Post(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>())).Callback((string name, string bio, DateTime dob, string gender) =>
            {
                entityList.Add(new Producer()
                {
                    Id = _id,
                    Name = name,
                    Gender = gender,
                    DOB = Convert.ToDateTime(dob),
                    Bio = bio
                });
                _id++;
            });

        }

        public static void MockPut()
        {
            List<Producer> entityList = new List<Producer>()
            {
                new Producer()
                {
                    Id=1,
                    Name="Kevin Feigi",
                    Gender="Male",
                    DOB= new DateTime(1973,06,02),
                    Bio="British"
                },
                new Producer()
                {
                    Id=2,
                    Name="Robert N. Fried",
                    Gender="Male",
                    DOB=new DateTime(1963,01,16),
                    Bio="Ukranian"

                },
                new Producer()
                {
                    Id=3,
                    Name="Kathryn Bigelow",
                    Gender="Female",
                    DOB=new DateTime(1951,11,27),
                    Bio="Ukranian"
                }
            };

            producerRepoMock.Setup(repo => repo.Put(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>())).Callback((int id, string name, string bio, DateTime dob, string gender) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
                entityList.Add(new Producer()
                {
                    Id = id,
                    Name = name,
                    Gender = gender,
                    DOB = Convert.ToDateTime(dob),
                    Bio = bio
                });
            });
        }

        public static void MockDelete()
        {
            List<Producer> entityList = new List<Producer>()
            {
                new Producer()
                {
                    Id=1,
                    Name="Kevin Feigi",
                    Gender="Male",
                    DOB= new DateTime(1973,06,02),
                    Bio="British"
                },
                new Producer()
                {
                    Id=2,
                    Name="Robert N. Fried",
                    Gender="Male",
                    DOB=new DateTime(1963,01,16),
                    Bio="Ukranian"

                },
                new Producer()
                {
                    Id=3,
                    Name="Kathryn Bigelow",
                    Gender="Female",
                    DOB=new DateTime(1951,11,27),
                    Bio="Ukranian"
                }
            };
            producerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>())).Callback((int id) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
            });
        }

        public static void MockGetByMovieId()
        {
            List<ProducerResponse> entityList = new List<ProducerResponse>()
            {
                new ProducerResponse()
                {
                    Id=1,
                    Name="Kevin Feigi",
                    
                },
                new ProducerResponse()
                {
                    Id=2,
                    Name="Robert N. Fried",
                }
            };
            producerRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
            {
                List<ProducerResponse> result = new();
                if (movieId == 1)
                {
                    result.Add(entityList.Find(entity => entity.Id == 2));
                }
                if (movieId == 2)
                {
                    result.Add(entityList.Find(entity => entity.Id == 1));
                }
                return result;
            });

        }






    }
}
