﻿using System.Collections.Generic;
using IMDBAPI.Models.DB;
using IMDBAPI.Repository;
using Moq;

namespace IMDBAPI.MockRepo
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> movieRepoMock = new Mock<IMovieRepository>();
        public static int _id = 3;
        public static void MockGetAll()
        {
            List<Movie> entityList = new()
            {
                new Movie()
                {
                    Id=1,
                    Name="Rudy",
                    YearOfRelease= 1993,
                    Plot= "Rudy has always been told that he was too small to play college football.",
                    ProducerId= 2,
                    CoverUrl= "url",
                },
                new Movie()
                {
                    Id=2,
                    Name="Iron Man",
                    YearOfRelease= 2008,
                    Plot= "Iron Man is a American superhero film based on the Marvel Comics character of the same name.",
                    ProducerId= 1,
                    CoverUrl= "url",
                }
            };
            movieRepoMock.Setup(repo => repo.GetAll()).Returns(() =>
            {
                return entityList;
            });
        }

        public static void MockGet()
        {
            List<Movie> entityList = new()
            {
                new Movie()
                {
                    Id = 1,
                    Name = "Rudy",
                    YearOfRelease = 1993,
                    Plot = "Rudy has always been told that he was too small to play college football.",
                    ProducerId = 2,
                    CoverUrl = "url",
                },
                new Movie()
                {
                    Id = 2,
                    Name = "Iron Man",
                    YearOfRelease = 2008,
                    Plot = "Iron Man is a American superhero film based on the Marvel Comics character of the same name.",
                    ProducerId = 1,
                    CoverUrl = "url",
                }
            };

            movieRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                List<Movie> result = new();
                result.Add(entityList.Find(item => item.Id == id));
                return result;
            });
        }

        public static void MockPost()
        {
            List<Movie> entityList = new()
            {
                new Movie()
                {
                    Id = 1,
                    Name = "Rudy",
                    YearOfRelease = 1993,
                    Plot = "Rudy has always been told that he was too small to play college football.",
                    ProducerId = 2,
                    CoverUrl = "url",
                },
                new Movie()
                {
                    Id = 2,
                    Name = "Iron Man",
                    YearOfRelease = 2008,
                    Plot = "Iron Man is a American superhero film based on the Marvel Comics character of the same name.",
                    ProducerId = 1,
                    CoverUrl = "url",
                }
            };
            movieRepoMock.Setup(repo => repo.Post(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<List<string>>(), It.IsAny<List<string>>())).Callback((string name, int yearOfRelease, string plot, int producerId,string coverUrl,List<string> actorIds,List<string> genreIds) =>
            {
                entityList.Add(new Movie()
                {
                    Id = _id,
                    Name = name,
                    YearOfRelease =yearOfRelease,
                    Plot=plot,
                    ProducerId=producerId,
                    CoverUrl=coverUrl
                });
                _id++;
            });

        }

        public static void MockPut()
        {
            List<Movie> entityList = new()
            {
                new Movie()
                {
                    Id = 1,
                    Name = "Rudy",
                    YearOfRelease = 1993,
                    Plot = "Rudy has always been told that he was too small to play college football.",
                    ProducerId = 2,
                    CoverUrl = "url",
                },
                new Movie()
                {
                    Id = 2,
                    Name = "Iron Man",
                    YearOfRelease = 2008,
                    Plot = "Iron Man is a American superhero film based on the Marvel Comics character of the same name.",
                    ProducerId = 1,
                    CoverUrl = "url",
                }
            };

            movieRepoMock.Setup(repo => repo.Put(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<List<string>>(), It.IsAny<List<string>>())).Callback((int id,string name, int yearOfRelease, string plot, int producerId, string coverUrl, List<string> actorIds, List<string> genreIds) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
                entityList.Add(new Movie()
                {
                    Id = _id,
                    Name = name,
                    YearOfRelease = yearOfRelease,
                    Plot = plot,
                    ProducerId = producerId,
                    CoverUrl = coverUrl
                });
            });
        }

        public static void MockDelete()
        {
            List<Movie> entityList = new()
            {
                new Movie()
                {
                    Id = 1,
                    Name = "Rudy",
                    YearOfRelease = 1993,
                    Plot = "Rudy has always been told that he was too small to play college football.",
                    ProducerId = 2,
                    CoverUrl = "url",
                },
                new Movie()
                {
                    Id = 2,
                    Name = "Iron Man",
                    YearOfRelease = 2008,
                    Plot = "Iron Man is a American superhero film based on the Marvel Comics character of the same name.",
                    ProducerId = 1,
                    CoverUrl = "url",
                }
            };
            movieRepoMock.Setup(repo => repo.Delete(It.IsAny<int>())).Callback((int id) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
            });

        }




    }
}
