﻿using System;
using System.Collections.Generic;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using Moq;

namespace IMDBAPI.MockRepo
{
    public class ActorMock
    {

        public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();
        public static int _id = 4; //3 actors previously available

        public static void MockGetAll()
        {
            List<Actor> entityList = new()
            {
                new Actor()
                {
                    Id = 1,
                    Name = "Robert Downey Jr.",
                    Gender = "Male",
                    DOB = new DateTime(1965, 04, 04),
                    Bio = "British"
                },
                new Actor()
                {
                    Id = 2,
                    Name = "Terrence Howard",
                    Gender = "Male",
                    DOB = new DateTime(1969, 03, 11),
                    Bio = "Ukranian"
                },
                new Actor()
                {

                    Id = 3,
                    Name = "Sean Patrick Astin",
                    Gender = "Male",
                    DOB = new DateTime(1971, 01, 25),
                    Bio = "Ukranian"
                }
            };

            actorRepoMock.Setup(repo => repo.GetAll()).Returns(() =>
            {
                return entityList;
            });
        }
        public static void MockGet()
        {
            List<Actor> entityList = new()
            {
                new Actor()
                {
                    Id = 1,
                    Name = "Robert Downey Jr.",
                    Gender = "Male",
                    DOB = new DateTime(1965, 04, 04),
                    Bio = "British"
                },
                new Actor()
                {
                    Id = 2,
                    Name = "Terrence Howard",
                    Gender = "Male",
                    DOB = new DateTime(1969, 03, 11),
                    Bio = "Ukranian"
                },
                new Actor()
                {

                    Id = 3,
                    Name = "Sean Patrick Astin",
                    Gender = "Male",
                    DOB = new DateTime(1971, 01, 25),
                    Bio = "Ukranian"
                }
            };

            actorRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                List<Actor> result = new();
                result.Add(entityList.Find(entity => entity.Id == id));
                return result;
            });
        }

        public static void MockPost()
        {
            List<Actor> entityList = new()
            {
                new Actor()
                {
                    Id = 1,
                    Name = "Robert Downey Jr.",
                    Gender = "Male",
                    DOB = new DateTime(1965, 04, 04),
                    Bio = "British"
                },
                new Actor()
                {
                    Id = 2,
                    Name = "Terrence Howard",
                    Gender = "Male",
                    DOB = new DateTime(1969, 03, 11),
                    Bio = "Ukranian"
                },
                new Actor()
                {

                    Id = 3,
                    Name = "Sean Patrick Astin",
                    Gender = "Male",
                    DOB = new DateTime(1971, 01, 25),
                    Bio = "Ukranian"
                }
            };

            actorRepoMock.Setup(repo => repo.Post(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>())).Callback((string name, string bio, DateTime dob, string gender) =>
            {
                entityList.Add(new Actor()
                {
                    Id = _id,
                    Name = name,
                    Gender = gender,
                    DOB = dob,
                    Bio = bio
                });
                _id++;
            });

        }

        public static void MockPut()
        {
            List<Actor> entityList = new()
            {
                new Actor()
                {
                    Id = 1,
                    Name = "Robert Downey Jr.",
                    Gender = "Male",
                    DOB = new DateTime(1965, 04, 04),
                    Bio = "British"
                },
                new Actor()
                {
                    Id = 2,
                    Name = "Terrence Howard",
                    Gender = "Male",
                    DOB = new DateTime(1969, 03, 11),
                    Bio = "Ukranian"
                },
                new Actor()
                {

                    Id = 3,
                    Name = "Sean Patrick Astin",
                    Gender = "Male",
                    DOB = new DateTime(1971, 01, 25),
                    Bio = "Ukranian"
                }
            };

            actorRepoMock.Setup(repo => repo.Put(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>())).Callback((int id,string name, string bio, DateTime dob, string gender) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
                entityList.Add(new Actor()
                {
                    Id = id,
                    Name = name,
                    Gender = gender,
                    DOB = Convert.ToDateTime(dob),
                    Bio = bio
                });
            });
        }

        public static void MockDelete()
        {
            List<Actor> entityList = new()
            {
                new Actor()
                {
                    Id = 1,
                    Name = "Robert Downey Jr.",
                    Gender = "Male",
                    DOB = new DateTime(1965, 04, 04),
                    Bio = "British"
                },
                new Actor()
                {
                    Id = 2,
                    Name = "Terrence Howard",
                    Gender = "Male",
                    DOB = new DateTime(1969, 03, 11),
                    Bio = "Ukranian"
                },
                new Actor()
                {

                    Id = 3,
                    Name = "Sean Patrick Astin",
                    Gender = "Male",
                    DOB = new DateTime(1971, 01, 25),
                    Bio = "Ukranian"
                }
            };

            actorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>())).Callback((int id) =>
            {
                entityList.Remove(entityList.Find(entity => entity.Id == id));
            });

        }

        public static void MockGetByMovieId()
        {
            List<ActorResponse> entityList = new()
            {
                new ActorResponse()
                {
                    Id = 1,
                    Name = "Robert Downey Jr.",
                },
                new ActorResponse()
                {
                    Id = 2,
                    Name = "Terrence Howard",
                }
            };
            actorRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
            {
                List<ActorResponse> result = new();
                if (movieId == 1)//movie 1 has actor 2(ids)
                {
                    result.Add(entityList.Find(entity => entity.Id == 2));
                }
                if(movieId == 2)
                {
                    result.Add(entityList.Find(entity => entity.Id == 1));
                }
                return result;
            });

        }


    }
}
