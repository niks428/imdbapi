﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBAPI.Models.Response
{
    public class ProducerResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
