﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBAPI.Models.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public int ProducerId { get; set; }
        public string CoverUrl { get; set; }

        public IEnumerable<ActorResponse> MovieActors { get; set; }

        public IEnumerable<ProducerResponse> MovieProducers { get; set; }

        public IEnumerable<GenreResponse> MovieGenres { get; set; }

    }
}
