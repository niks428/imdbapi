﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Repository
{
    public interface IActorRepository
    {
        public IEnumerable<Actor> GetAll();

        public IEnumerable<Actor> Get(int id);


        public void Post(
           string Name,
           string Bio,
           DateTime DOB,
           string Gender);


        public void Put(
            int id,
            string Name,
            string Bio,
            DateTime DOB,
            string Gender
            );

        public void Delete(int id);

        public IEnumerable<ActorResponse> GetByMovieId(int movieId);

    }
}
