﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace IMDBAPI.Repository
{
    public class BaseRepository<T>  where T : class
    {

        private readonly ConnectionString _connectionString;

        public BaseRepository(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<T> GetAll(string query)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<T>(query);
        }

    }
}
