﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using IMDBAPI.Models.DB;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
	public class MovieRepository : IMovieRepository
	{
		private readonly ConnectionString _connectionString;

		public MovieRepository(IOptions<ConnectionString> connectionString) //: base(connectionString.Value)
		{
			_connectionString = connectionString.Value;
		}

		public IEnumerable<Movie> GetAll()
		{
			const string query = @"
SELECT Id
	,Name
	,YearOfRelease
	,Plot
	,ProducerId
	,CoverUrl
FROM Movies";
			using var connection = new SqlConnection(_connectionString.DB);
			var movies = connection.Query<Movie>(query);
			return movies;
		}

		public IEnumerable<Movie> Get(int id)
		{
			const string query = @"
SELECT Id
	,Name
	,YearOfRelease
	,Plot
	,ProducerId
	,CoverUrl
FROM Movies 
WHERE Id= @id ";

			using var connection = new SqlConnection(_connectionString.DB);
			var movie = connection.Query<Movie>(query, new { Id = id });
			return movie;

		}


		public void Post(string Name,int YearOfRelease,string Plot,int ProducerId,string CoverUrl,List<string> Actors,List<string> Genres)
		{
			var movie = new
			{
				Name = Name,
				YearOfRelease = YearOfRelease,
				Plot = Plot,
				ProducerId = ProducerId,
				CoverUrl = CoverUrl,
				ActorIds = string.Join(",", Actors),
				GenreIds = string.Join(",", Genres)
			};

			
			const string query = @"
EXEC usp_AddMovie @Name
	,@YearOfRelease
	,@Plot
	,@ProducerId
	,@CoverUrl
	,@ActorIds
	,@GenreIds";
			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, movie);
		}

		public void Put(int Id, string Name, int YearOfRelease, string Plot, int ProducerId, string CoverUrl, List<string> Actors, List<string> Genres)
		{
			var movie = new
			{
				Id= Id,
				Name = Name,
				YearOfRelease = YearOfRelease,
				Plot = Plot,
				ProducerId = ProducerId,
				CoverUrl = CoverUrl,
				ActorIds = string.Join(",", Actors),
				GenreIds = string.Join(",", Genres)
			};

			const string query = @"
EXEC usp_UpdateMovie @Id
	,@Name
	,@YearOfRelease
	,@Plot
	,@ProducerId
	,@CoverUrl
	,@ActorIds
	,@GenreIds";

			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, movie);

		}


		public void Delete(int id)
		{
			const string query = @"
EXEC usp_DeleteMovie @Id";

			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, new { Id = id });
		}



	}
}