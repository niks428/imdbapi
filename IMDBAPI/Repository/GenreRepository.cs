﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class GenreRepository : IGenreRepository
    {

        private readonly ConnectionString _connectionString;

        public GenreRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }


        public IEnumerable<Genre> GetAll()
        {
            const string sql = @"
SELECT Id
	,Name
FROM Genres";

            using var connection = new SqlConnection(_connectionString.DB);
            var genres = connection.Query<Genre>(sql);
            return genres;
        }

        public IEnumerable<Genre> Get(int Id)
        {
            const string query = @"
SELECT *
FROM Genres 
WHERE Id= @id ";

            using var connection = new SqlConnection(_connectionString.DB);
            var genre = connection.Query<Genre>(query, new { Id });
            return genre;
        }

        public void Post(string Name)
        {
            const string query = @"
INSERT INTO 
Genres
Values(@Name)";
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query(query, new { Name });
        }


        public void Put(int Id, string Name)
        {
            var genre = new{Id, Name};

            const string query = @"
UPDATE Genres
SET Name = @Name
WHERE ID = @Id";

            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query(query, genre);
        }


        public void Delete(int Id)
        {
            const string query = @"
EXEC usp_DeleteGenre @Id";

            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query(query, new { Id });
        }

        public IEnumerable<GenreResponse> GetByMovieId(int movieId)
        {
            const string sql = @"
SELECT G.Id
	,G.Name
FROM Genres G
INNER JOIN MovieGenreMapping MGM ON G.Id = MGM.GenreId
WHERE MGM.MovieId = @MovieId";

            using var connection = new SqlConnection(_connectionString.DB);
            var genres = connection.Query<GenreResponse>(sql, new { MovieId = movieId });
            return genres;
        }
    }
}
