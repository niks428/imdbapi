﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Repository
{
    public interface IGenreRepository
    {
        public IEnumerable<Genre> GetAll();

        public IEnumerable<Genre> Get(int id);


        public void Post(string Name);


        public void Put(int id,string Name);

        public void Delete(int id);

        public IEnumerable<GenreResponse> GetByMovieId(int movieId);

    }
}
