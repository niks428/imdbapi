﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
	public class ProducerRepository : IProducerRepository
	{

		private readonly ConnectionString _connectionString;

		public ProducerRepository(IOptions<ConnectionString> connectionString)
		{
			_connectionString = connectionString.Value;
		}


		public IEnumerable<Producer> GetAll()
		{
			const string sql = @"
SELECT Id
	,Name
	,Bio
	,DOB
	,Gender
FROM Producers";

			using var connection = new SqlConnection(_connectionString.DB);
			var producers = connection.Query<Producer>(sql);
			return producers;
		}

		public IEnumerable<Producer> Get(int Id)
		{
			const string query = @"
SELECT Id
	,Name
	,Bio
	,DOB
	,Gender
FROM Producers 
WHERE Id= @id ";

			using var connection = new SqlConnection(_connectionString.DB);
			var producer = connection.Query<Producer>(query, new { Id });
			return producer;
		}

		public void Post(string Name, string Bio, DateTime DOB, string Gender)
		{
			var producer = new
			{
				Name,
				Bio,
				DOB,
				Gender
			};

			const string query = @"
INSERT INTO 
Producers
Values(@Name,@Bio,@DOB,@Gender)";
			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, producer);
		}


		public void Put(int Id, string Name, string Bio, DateTime DOB, string Gender)
		{
			var producer = new
			{
				Id,
				Name,
				Bio,
				DOB,
				Gender
			};

			const string query = @"
UPDATE Producers
SET Name = @Name
	,Bio = @Bio
	,DOB = @DOB
	,Gender = @Gender
WHERE ID = @Id";

			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, producer);
		}


		public void Delete(int Id)
		{
			const string query = @"
EXEC usp_DeleteProducer @Id";

			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, new { Id });
		}

		public IEnumerable<ProducerResponse> GetByMovieId(int movieId)
		{
			const string sql = @"
SELECT P.Id
	,P.Name
FROM Producers P
INNER JOIN Movies M 
ON M.ProducerId = P.Id
WHERE M.Id = @MovieId";

			using var connection = new SqlConnection(_connectionString.DB);
			var producers = connection.Query<ProducerResponse>(sql, new { MovieId = movieId });
			return producers;
		}
	}
}
