﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Repository
{
    public interface IMovieRepository
    {
        public void Post(
            string Name, 
            int YearOfRelease, 
            string Plot, 
            int ProducerId, 
            string CoverUrl, 
            List<string> Actors,
            List<string> Genres);
        public IEnumerable<Movie> GetAll();
        public IEnumerable<Movie> Get(int id);


        public void Put(
            int id,
            string Name,
            int YearOfRelease,
            string Plot,
            int ProducerId,
            string CoverUrl,
            List<string> Actors,
            List<string> Genres
            );

        public void Delete(int id);

    }
}
