﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Response;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class ActorRepository : IActorRepository //BaseRepository<Actor>,
    {
        private readonly ConnectionString _connectionString;

        public ActorRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }


        public IEnumerable<Actor> GetAll()
        {
            const string sql = @"
SELECT Id
	,Name
	,Bio
	,DOB
	,Gender
FROM Actors";

            using var connection = new SqlConnection(_connectionString.DB);
            var actors = connection.Query<Actor>(sql);
            return actors;
        }


        public IEnumerable<Actor> Get(int id)
        {
            const string query = @"
SELECT Id
	,Name
	,Bio
	,DOB
	,Gender
FROM Actors 
WHERE Id= @id ";

            using var connection = new SqlConnection(_connectionString.DB);
            var actor = connection.Query<Actor>(query, new { Id = id });
            return actor;
        }


        public void Post(string Name, string Bio, DateTime DOB, string Gender)
        {
            var actor = new
            {
                Name,
                Bio,
                DOB,
                Gender
            };

            const string query = @"
INSERT INTO 
Actors
Values(@Name,@Bio,@DOB,@Gender)";
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query(query, actor);
        }



        public void Put(int Id, string Name, string Bio, DateTime DOB, string Gender)
    {
            var actor = new
            {
                Id,
                Name,
                Bio,
                DOB,
                Gender
            };

            const string query = @"
UPDATE Actors
SET Name = @Name
    ,Bio = @Bio
    ,DOB = @DOB
    ,Gender = @Gender
WHERE ID = @Id";

            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query(query, actor);
        }


        public void Delete(int id)
        {
            const string query = @"
EXEC usp_DeleteActor @Id";

            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query(query, new { Id = id });
        }

        public IEnumerable<ActorResponse> GetByMovieId(int movieId)
        {
            const string sql = @"
SELECT A.Id
	,A.Name
FROM Actors A
INNER JOIN MovieActorMapping MAM ON A.Id = MAM.ActorId
WHERE MAM.MovieId = @MovieId";

            using var connection = new SqlConnection(_connectionString.DB);
            var actors = connection.Query<ActorResponse>(sql,new { MovieId=movieId });
            return actors;
        }
    }
}