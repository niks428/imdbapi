﻿using FluentValidation;
using IMDBAPI.Models.Request;

namespace IMDBAPI.Validators
{
    public class GenreRequestValidator : AbstractValidator<GenreRequest>
    {
        public GenreRequestValidator()
        {
            RuleFor(x => x.Name)
              .NotEmpty()
              .MaximumLength(80)
              .Matches("^[a-zA-Z ]*$");
        }    
    }
}
