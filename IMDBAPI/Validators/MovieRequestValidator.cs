﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using IMDBAPI.Models.Request;

namespace IMDBAPI.Validators
{
    public class MovieRequestValidator : AbstractValidator<MovieRequest>
    {
        public MovieRequestValidator()
        {
            RuleFor(x => x.Name)
              .NotEmpty()
              .Matches("^[a-zA-Z ]*$")
              .MaximumLength(80);

            RuleFor(x => x.YearOfRelease)
                .NotEmpty()
                .GreaterThan(1900)
                .LessThan(DateTime.Now.Year);

            RuleFor(x => x.ProducerId)
                .NotEmpty();

            RuleFor(x => x.Actors)
              .NotEmpty();
            RuleFor(x => x.Genres)
                .NotEmpty();

            
        }
    }
}
