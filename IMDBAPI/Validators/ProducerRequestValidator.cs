﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using IMDBAPI.Models.Request;

namespace IMDBAPI.Validators
{
    public class ProducerRequestValidator : AbstractValidator<ProducerRequest>
    {
        public ProducerRequestValidator()
        {
            RuleFor(x => x.Name)
              .NotEmpty()
              .MaximumLength(40)
              .Matches("^[a-zA-Z ]*$");

            RuleFor(x => x.DOB)
                .NotEmpty()
                .LessThanOrEqualTo(DateTime.Now);

            RuleFor(x => x.Gender)
                .NotEmpty()
                .MaximumLength(20)
                .Matches("^[a-zA-Z]*$");
        }
    }
}
