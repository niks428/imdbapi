﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Controllers
{
    [ApiController]
    [GlobalExceptionHandler]
    [Route("genres")]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var genres = _genreService.GetAll();
            return new JsonResult(genres);
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var genre = _genreService.Get(id);
            return new JsonResult(genre);
        }



        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genre)
        {
            _genreService.Post(genre);
            return Ok();
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] GenreRequest genre)
        {
            
            _genreService.Put(id, genre);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _genreService.Delete(id);

            return Ok();
        }


    }
}
