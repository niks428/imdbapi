﻿using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Controllers
{
    [ApiController]
    [GlobalExceptionHandler]
    [Route("producers")]
    public class ProducerController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;

        }
        
        [HttpGet]
        public IActionResult GetAll()
        {
            var producers = _producerService.GetAll();
            return new JsonResult(producers);
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var producer = _producerService.Get(id);
            return new JsonResult(producer);
        }


        [HttpPost]
        public IActionResult Post([FromBody] ProducerRequest producer)
        {
            _producerService.Post(producer);
            return Ok();
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProducerRequest producer)
        {

            _producerService.Put(id, producer);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _producerService.Delete(id);

            return Ok();
        }

    }
}
