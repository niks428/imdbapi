﻿
using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Controllers
{
    [ApiController]
    [GlobalExceptionHandler]
    [Route("movies")]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IConfiguration _config; //default config
        private readonly ReadUrl _readUrl;



        public MovieController(IMovieService movieService, IConfiguration config,IOptions<ReadUrl> readUrl)
        {
            _movieService = movieService;
             _config = config;
            _readUrl = readUrl.Value;

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var movies = _movieService.GetAll();
            return new JsonResult(movies);
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var movie = _movieService.Get(id);
            return new JsonResult(movie);
        }



        [HttpPost]
        public IActionResult Post([FromBody] MovieRequest movie) 
        {
            _movieService.Post(movie);
            return Ok();
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] MovieRequest movie)
        {
            _movieService.Put(id, movie); 

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _movieService.Delete(id);
            return Ok();
        }

        //Getting url from Config File
        [HttpGet("url")]
        public IActionResult GetUrl()
        {
            var url1 = _config.GetValue<string>("ReadUrl:ReadConfig");
            var url2 = _readUrl.OptionsPattern;
            return new JsonResult(url2);
        }


}
}
