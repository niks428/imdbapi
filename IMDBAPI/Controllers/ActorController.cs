﻿using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Controllers
{
    [ApiController]
    [GlobalExceptionHandler]
    [Route("actors")]
    public class ActorController : ControllerBase
    {
        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        /*[HttpGet]
        public IActionResult GetAll()
        {
            var actors = _actorService.GetAll();
            return new JsonResult(actors);
        }*/

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var actor = _actorService.Get(id);
            return new JsonResult(actor);

        }

        [HttpPost]
        public IActionResult Post([FromBody] ActorRequest actor)
        {
            _actorService.Post(actor);
            return Ok(actor.Name);
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ActorRequest actor)
        {

            _actorService.Put(id, actor);

            return Ok(id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _actorService.Delete(id);

            return Ok(id);
        }

        [HttpGet]
        public IActionResult GetByParams([FromQuery] int id=0)
        {

            if (id == 0)
            {
                var actor = _actorService.GetAll();
                return new JsonResult(actor);
            }

            if (id != 0)
            {
                var actor = _actorService.Get(id);
                return new JsonResult(actor);
            }

            return Ok();

        }




    }
}
