﻿using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;

namespace IMDBAPI.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _repository;

        public GenreService(IGenreRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<GenreResponse> GetAll()
        {
            var genres = _repository.GetAll().Where(genre => genre != null).Select(genre => new GenreResponse
            {
                Id = genre.Id,
                Name = genre.Name
            });

            if (!genres.Any())
            {
                throw new EntityNotFoundException("No Actors Available!");
            }

            return genres;
        }

        public IEnumerable<GenreResponse> Get(int id)
        {
            var genre = _repository.Get(id).Where(genre => genre != null).Select(genre => new GenreResponse
            {
                Id = genre.Id,
                Name = genre.Name
            });

            if (!genre.Any())
            {
                throw new EntityNotFoundException(string.Format("Genre with Id {0} Not Available", id));
            }

            return genre;
        }

        public void Post(GenreRequest genre)
        {
            _repository.Post(genre.Name);
        }

        public void Put(int id, GenreRequest genre)
        {
            _repository.Put(id,genre.Name);
        }

        public void Delete(int id)
        {

            var genre = _repository.Get(id);
            if (!genre.Any())
            {
                throw new EntityNotFoundException(string.Format("Genre with Id {0} Not Available", id));
            }
            _repository.Delete(id);
        }
    }
}
