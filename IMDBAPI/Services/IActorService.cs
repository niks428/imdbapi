﻿using System.Collections.Generic;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Services
{
    public interface IActorService
    {
        public IEnumerable<ActorResponse> GetAll();

        public IEnumerable<ActorResponse> Get(int id);

        public void Post(ActorRequest actor);

        public void Put(int id, ActorRequest actor);

        public void Delete(int id);


    }


}
