﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Exceptions;
using IMDBAPI.Models.DB;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;


namespace IMDBAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IProducerRepository _producerRepository;
        private readonly IGenreRepository _genreRepository;

        public MovieService(IMovieRepository movieRepository,IActorRepository actorRepository,IProducerRepository producerRepository,IGenreRepository genreRepository)
        {

            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _producerRepository = producerRepository;
            _genreRepository = genreRepository;
        }

        public IEnumerable<MovieResponse> GetAll()
        {
            var movieResponses = new List<MovieResponse>();
            var movies = _movieRepository.GetAll().Where(movie => movie != null);

            if (!movies.Any())
            {
                throw new EntityNotFoundException("No Movies Available!");
            }

            foreach (var movie in movies)
            {
                var movieResponse = new MovieResponse();
                movieResponse.Id = movie.Id;
                movieResponse.Name = movie.Name;
                movieResponse.YearOfRelease = movie.YearOfRelease;
                movieResponse.Plot = movie.Plot;
                movieResponse.ProducerId = movie.ProducerId;
                movieResponse.CoverUrl = movie.CoverUrl;
                movieResponse.MovieActors = _actorRepository.GetByMovieId(movie.Id);
                movieResponse.MovieProducers = _producerRepository.GetByMovieId(movie.Id);
                movieResponse.MovieGenres = _genreRepository.GetByMovieId(movie.Id);
                movieResponses.Add(movieResponse);
            }

            
            return movieResponses;
        }

        public IEnumerable<MovieResponse> Get(int id)
        {

            var movieResponses = new List<MovieResponse>();
            var movies = _movieRepository.Get(id).Where(movie => movie != null);

            if (!movies.Any())
            {
                throw new EntityNotFoundException(string.Format("Movie with Id {0} Not Available", id));
            }

            foreach (var movie in movies)
            {
                var movieResponse = new MovieResponse();
                movieResponse.Id = movie.Id;
                movieResponse.Name = movie.Name;
                movieResponse.YearOfRelease = movie.YearOfRelease;
                movieResponse.Plot = movie.Plot;
                movieResponse.ProducerId = movie.ProducerId;
                movieResponse.CoverUrl = movie.CoverUrl;
                movieResponse.MovieActors = _actorRepository.GetByMovieId(movie.Id);
                movieResponse.MovieProducers = _producerRepository.GetByMovieId(movie.Id);
                movieResponse.MovieGenres = _genreRepository.GetByMovieId(movie.Id);
                movieResponses.Add(movieResponse);
            }
            return movieResponses;
        }


        public void Post(MovieRequest movie)
        {
            _movieRepository.Post(
                movie.Name,
                movie.YearOfRelease,
                movie.Plot,
                movie.ProducerId,
                movie.CoverUrl,
                movie.Actors,
                movie.Genres
                );
        }

         

        public void Put(int id,MovieRequest movie)
        {
            _movieRepository.Put(
                id,
                movie.Name,
                movie.YearOfRelease,
                movie.Plot,
                movie.ProducerId,
                movie.CoverUrl,
                movie.Actors,
                movie.Genres
                );
        }

        public void Delete(int id)
        {
            var movie = _movieRepository.Get(id);
            if (!movie.Any())
            {
                throw new EntityNotFoundException(string.Format("Movie with Id {0} Not Available", id));
            }

            _movieRepository.Delete(id);
        }
    }
}