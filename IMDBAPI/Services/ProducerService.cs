﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;

namespace IMDBAPI.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _repository;

        public ProducerService(IProducerRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<ProducerResponse> GetAll()
        {
        var producers = _repository.GetAll().Where(producer => producer != null).Select(producer => new ProducerResponse
        {
            Id = producer.Id,
            Name = producer.Name
        });

            if (!producers.Any())
            {
                throw new EntityNotFoundException("No Producers Available!");
            }
            return producers;
        }

        public IEnumerable<ProducerResponse> Get(int id)
        {
            var producer = _repository.Get(id).Where(producer => producer != null).Select(producer => new ProducerResponse
            {
                Id = producer.Id,
                Name = producer.Name
            });

            if (!producer.Any())
            {
                throw new EntityNotFoundException(string.Format("Producer with Id {0} Not Available", id));
            }
            return producer;
        }

        public void Post(ProducerRequest producer)
        {
            _repository.Post(
                producer.Name,
                producer.Bio,
                producer.DOB,
                producer.Gender
                );
        }

        public void Put(int id, ProducerRequest producer)
        {
            _repository.Put(
                id,
                producer.Name,
                producer.Bio,
                producer.DOB,
                producer.Gender
                );
        }

        public void Delete(int id)
        {
            var producer = _repository.Get(id);

            if (!producer.Any())
            {
                throw new EntityNotFoundException(string.Format("Producer with Id {0} Not Available", id));
            }
            _repository.Delete(id);
        }

    }

}
