﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using IMDBAPI.Exceptions;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;

namespace IMDBAPI.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _repository;

    public ActorService(IActorRepository repository)
    {
        _repository = repository;
    }

        public IEnumerable<ActorResponse> GetAll()
        {
            
            var actors = _repository.GetAll().Select(actor => new ActorResponse
                {
                    Id = actor.Id,
                    Name = actor.Name,
                });
            
                

            if (!actors.Any())
            {
                throw new EntityNotFoundException("No Actors Available!");
            }

            return actors;
        }

        public IEnumerable<ActorResponse> Get(int id) 

        {
            var actor = _repository.Get(id).Where(actor => actor != null).Select(actor => new ActorResponse
            {
                Id = actor.Id,
                Name = actor.Name
            });

            if (!actor.Any())
            {
                throw new EntityNotFoundException(string.Format("Actor with Id {0} Not Available", id));

            }
            return actor;
        }


        public void Post(ActorRequest actor)
        {
            _repository.Post(
                actor.Name,
                actor.Bio,
                actor.DOB,
                actor.Gender
                );
        }

        public void Put(int id, ActorRequest actor)
        {
            var tempActor = _repository.Get(id).Select(actor => new ActorResponse
            {
                Id = actor.Id,
                Name = actor.Name
            });

            if (!tempActor.Any())
            {
                throw new EntityNotFoundException(string.Format("Actor with Id {0} Not Available", id));

            }
            _repository.Put(
                id,
                actor.Name,
                actor.Bio,
                actor.DOB,
                actor.Gender
                );
        }

        public void Delete(int id)
        {
            var actor = _repository.Get(id);
            if (!actor.Any())
            {
                throw new EntityNotFoundException(string.Format("Actor with Id {0} Not Available",id));
            }
            _repository.Delete(id);
        }

        
    }
}