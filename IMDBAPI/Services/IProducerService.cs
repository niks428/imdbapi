﻿using System.Collections.Generic;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Services
{
    public interface IProducerService
    {
        public IEnumerable<ProducerResponse> GetAll();

        public IEnumerable<ProducerResponse> Get(int id);

        public void Post(ProducerRequest producer);

        public void Put(int id, ProducerRequest producer);

        public void Delete(int id);
    }
}
