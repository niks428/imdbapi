﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Services
{
    public interface IMovieService
    {
        public IEnumerable<MovieResponse> GetAll();
        public IEnumerable<MovieResponse> Get(int id);

        public void Post(MovieRequest movie);

        public void Put(int id, MovieRequest movie);

        public void Delete(int id);

    }
}
