﻿using System.Collections.Generic;
using IMDBAPI.Models.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Services
{
    public interface IGenreService
    {
        public IEnumerable<GenreResponse> GetAll();

        public IEnumerable<GenreResponse> Get(int id);

        public void Post(GenreRequest genre);

        public void Put(int id, GenreRequest genre);

        public void Delete(int id);
    }
}
