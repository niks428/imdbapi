﻿namespace IMDBAPI.Exceptions
{
    internal class CustomError
    {
        private object code;
        private string v;

        public CustomError(object code, string v)
        {
            this.code = code;
            this.v = v;
        }
    }
}