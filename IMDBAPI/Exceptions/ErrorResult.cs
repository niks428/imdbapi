﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace IMDBAPI.Exceptions
{
    public class ErrorResult : JsonResult
    {
        public ErrorResult(ErrorType type, Exception exception)
                : base(null)
        {
            if (exception is null)
            {
                throw new ArgumentNullException(nameof(exception));
            }

            StatusCode = (int)HttpStatusCode.InternalServerError;
            Value = "Internal Server Error. Contact to Admin";

            switch (type)
            {
                case ErrorType.EntityNotFoundException:
                    StatusCode = (int)HttpStatusCode.NotFound;
                    Value = exception.Message.ToString();
                    break;
              
                default:
                    break;
            }

            
                
        }
    }
}
