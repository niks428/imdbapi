﻿

using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace IMDBAPI.Exceptions
{
    public class GlobalExceptionHandler : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }


            var ex = context.Exception;
            switch (ex)
            {
                case EntityNotFoundException:
                    context.Result = new ErrorResult(ErrorType.EntityNotFoundException, ex);
                    break;
            }

            base.OnException(context);
        }
    }
}

