﻿using IMDBAPI;
using IMDBAPI.MockRepo;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDBAPI_Test.Steps
{
    [Scope(Feature = "Producer Test")]
    [Binding]
    class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder => {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => ProducerMock.producerRepoMock.Object);
                });
            }))
        {

        }

        [BeforeScenario]
        public void MockRepositries()
        {
            ProducerMock.MockGetAll();
            ProducerMock.MockGet();
            ProducerMock.MockPost();
            ProducerMock.MockPut();
            ProducerMock.MockDelete();
        }


    }
}
