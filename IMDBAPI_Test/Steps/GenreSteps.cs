﻿using IMDBAPI;
using IMDBAPI.MockRepo;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDBAPI_Test.Steps
{
    [Scope(Feature = "Genre Test")]
    [Binding]
    class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder => {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => GenreMock.genreRepoMock.Object);
                });
            }))
        {

        }

        [BeforeScenario]
        public void MockRepositries()
        {
            GenreMock.MockGetAll();
            GenreMock.MockGet();
            GenreMock.MockPost();
            GenreMock.MockPut();
            GenreMock.MockDelete();
        }
    }
}
