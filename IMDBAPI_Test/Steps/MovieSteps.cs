﻿using IMDBAPI;
using IMDBAPI.MockRepo;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDBAPI_Test.Steps
{
    [Scope(Feature = "Movie Test")]
    [Binding]
    class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => MovieMock.movieRepoMock.Object);
                    services.AddScoped(service => ActorMock.actorRepoMock.Object);
                    services.AddScoped(service => ProducerMock.producerRepoMock.Object);
                    services.AddScoped(service => GenreMock.genreRepoMock.Object);
                });
            }))
        {

        }

        [BeforeScenario]
        public void MockRepositries()
        {
            MovieMock.MockGetAll();
            MovieMock.MockGet();
            MovieMock.MockPost();
            MovieMock.MockPut();
            MovieMock.MockDelete();
            ActorMock.MockGetByMovieId();
            ProducerMock.MockGetByMovieId();
            GenreMock.MockGetByMovieId();
        }
    }
}
