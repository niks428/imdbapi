﻿

using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using IMDBAPI;
using Microsoft.AspNetCore.Mvc.Testing;
using TechTalk.SpecFlow;
using Xunit;
using System.Text;
using Newtonsoft.Json;

namespace IMDBAPI_Test.Steps
{
    [Binding]
    public class BaseSteps
    {
        protected HttpClient Client { get; set; }
        protected WebApplicationFactory<TestStartup> Factory;
        protected HttpResponseMessage httpResponseMessage;
        public HttpContent httpContent;
        protected StringContent stringContent;

        public BaseSteps(WebApplicationFactory<TestStartup> baseFactory )
        {
            Factory = baseFactory;

        }

        [Given(@"I am a client")]
        public void GivenIAmAClient()
        {
            Client = Factory.CreateClient(new WebApplicationFactoryClientOptions
            { 
                BaseAddress= new Uri("http://localhost")
            });
        }

        [When(@"I Make GET Request '(.*)'")]
        public async Task WhenIMakeGETRequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.GetAsync(uri);
        }

       

        [Then(@"response code must be '(.*)'")]
        public void ThenResponseCodeMustBe(int statusCode)
        {
            var expectedStatusCode = (HttpStatusCode)statusCode;
            Assert.Equal(expectedStatusCode, httpResponseMessage.StatusCode);
        }

        [Then(@"response data must look like '(.*)'")]
        public async Task ThenResponseDataMustLookLike(string expectedResponse)
        {
            Console.WriteLine(httpResponseMessage.ToString());
            var responseData = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            Assert.Equal(expectedResponse, responseData.ToString());
        }


        [When(@"I Make POST Request '(.*)'")]
        public async Task WhenIMakePOSTRequestAsync(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.PostAsync(uri,stringContent).ConfigureAwait(false);
        }

        

        [When(@"data items are '(.*)'")]
        public void WhenDataItemsAre(string json)
        {
            stringContent = new StringContent(json, Encoding.UTF8, "application/json");
        }


        [When(@"I Make PUT Request '(.*)'")]
        public async Task WhenIMakePUTRequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.PutAsync(uri, stringContent);
        }


        [When(@"I Make DELETE Request '(.*)'")]
        public async Task WhenIMakeDELETERequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.GetAsync(uri);
        }

        



    }
}
