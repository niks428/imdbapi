﻿using IMDBAPI;
using IMDBAPI.MockRepo;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using IMDBAPI.Models.Request;


namespace IMDBAPI_Test.Steps
{

    [Scope(Feature = "Actor Test")]
    [Binding]
    public class ActorSteps : BaseSteps 
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder => {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => ActorMock.actorRepoMock.Object);
                });
            }))
        {

        }


        [BeforeScenario("Get All Actors")]
        public void GetAll()
        {
            ActorMock.MockGetAll();
        }

        [BeforeScenario("Get Actor By Id")]
        public void Get()
        {
            ActorMock.MockGet();
        }

        [BeforeScenario("Delete Actor By Id")]
        [BeforeScenario("Delete Unavailable Actor By Id")]
        public void Delete()
        {
            ActorMock.MockDelete();
        }

        [BeforeScenario("Add Actor")]
        public void Post()
        {
            ActorMock.MockPost();
        }

        [BeforeScenario("Update Actor By Id")]
        public void Put()
        {
            ActorMock.MockPut();
        }

        




    }
}
