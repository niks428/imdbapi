﻿using IMDBAPI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;

namespace IMDBAPI_Test
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TestStartup>
    {
        protected override IHostBuilder CreateHostBuilder()
        {
            return base.CreateHostBuilder()
            .ConfigureWebHostDefaults(webBuilder => {
                webBuilder.UseStartup<TestStartup>();
            });
        }


    }
}
