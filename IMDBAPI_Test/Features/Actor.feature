﻿Feature: Actor Test

@getAllActors
Scenario: Get All Actors
	Given I am a client
	When I Make GET Request '/actors'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Robert Downey Jr."},{"id":2,"name":"Terrence Howard"},{"id":3,"name":"Sean Patrick Astin"}]'

#@getActorById
#Scenario: Get Actor By Id
#	Given I am a client
#	When I Make GET Request '/actors/1'
#	Then response code must be '200'
#	And response data must look like '[{"id":1,"name":"Robert Downey Jr."}]'

@PostActor
Scenario: Add Actor
	Given I am a client
	When data items are '{"name":"Salman Khan","Bio":"Bollywood actor","DOB":"1969-03-11","Gender":"Male"}'
	And I Make POST Request '/actors/'
	Then response code must be '200'


@PutActor
Scenario: Update Actor By Id
	Given I am a client
	When data items are '{"name":"Salman Khan","Bio":"Bollywood actor","DOB":"1969-03-11","Gender":"Male"}'
	And I Make PUT Request '/actors/1/'
	Then response code must be '200'



@DeleteActorById
Scenario: Delete Actor By Id
	Given I am a client
	When I Make DELETE Request '/actors/2'
	Then response code must be '200'

@PostInvalidActor
Scenario: Add Invalid Actor
	Given I am a client
	When data items are '{"name":"121","Bio":"Bollywood actor","DOB":"1969-03-11","Gender":"Male"}'
	And I Make POST Request '/actors/'
	Then response code must be '400'

@DeleteUnavailableActorById
Scenario: Delete Unavailable Actor By Id
	Given I am a client
	When I Make DELETE Request '/actors/-1'
	Then response code must be '404'
	And response data must look like '"Actor with Id -1 Not Available"'


@GetUnavailableActorById
Scenario: Get Unavailable Actor By Id
	Given I am a client
	When I Make GET Request '/actors/-1'
	Then response code must be '404'
	And response data must look like '"Actor with Id -1 Not Available"'



@getActorByQueryParameter
Scenario: Get Actor By Query Parameter
	Given I am a client
	When I Make GET Request '/actors?id=1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Robert Downey Jr."}]'