﻿Feature: Producer Test

@getAllProducers
Scenario: Get All Producers
	Given I am a client
	When I Make GET Request '/producers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Kevin Feigi"},{"id":2,"name":"Robert N. Fried"},{"id":3,"name":"Kathryn Bigelow"}]'

@getProducerById
Scenario: Get Producer By Id
	Given I am a client
	When I Make GET Request '/producers/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Kevin Feigi"}]'


@PostProducer
Scenario: Add Producer
	Given I am a client
	When data items are '{"name":"Salman Khan","Bio":"Bollywood Producer/Producer","DOB":"1969-03-11","Gender":"Male"}'
	And I Make POST Request '/producers/'
	Then response code must be '200'

@PutProducer
Scenario: Update Producer By Id
	Given I am a client
	When data items are '{"name":"Karan Johar","Bio":"Bollywood Producer/Producer","DOB":"1969-03-11","Gender":"Male"}'
	And I Make PUT Request '/producers/1/'
	Then response code must be '200'

@DeleteProducerById
Scenario: Delete Producer By Id
	Given I am a client
	When I Make DELETE Request '/producers/2'
	Then response code must be '200'


@PostInvalidProducer
Scenario: Add Invalid Producer
	Given I am a client
	When data items are '{"name":"Salman Khan","Bio":"Bollywood Producer/Producer","DOB":"2076-03-11","Gender":"Male"}'
	And I Make POST Request '/producers/'
	Then response code must be '400'

@DeleteUnavailableProducerById
Scenario: Delete Unavailable Producer By Id
	Given I am a client
	When I Make DELETE Request '/producers/-1'
	Then response code must be '404'
	And response data must look like '"Producer with Id -1 Not Available"'


@GetUnavailableProducerById
Scenario: Get Unavailable Producer By Id
	Given I am a client
	When I Make GET Request '/producers/-1'
	Then response code must be '404'
	And response data must look like '"Producer with Id -1 Not Available"'

