﻿Feature: Movie Test

@getAllMovies
Scenario: Get All Movies
	Given I am a client
	When I Make GET Request '/movies/'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Rudy","yearOfRelease":1993,"plot":"Rudy has always been told that he was too small to play college football.","producerId":2,"coverUrl":"url","movieActors":[{"id":2,"name":"Terrence Howard"}],"movieProducers":[{"id":2,"name":"Robert N. Fried"}],"movieGenres":[{"id":5,"name":"Sports"},{"id":6,"name":"Drama"}]},{"id":2,"name":"Iron Man","yearOfRelease":2008,"plot":"Iron Man is a American superhero film based on the Marvel Comics character of the same name.","producerId":1,"coverUrl":"url","movieActors":[{"id":1,"name":"Robert Downey Jr."}],"movieProducers":[{"id":1,"name":"Kevin Feigi"}],"movieGenres":[{"id":1,"name":"Action"},{"id":2,"name":"War"},{"id":3,"name":"Superhero"},{"id":4,"name":"Science Fiction"}]}]'

	
@getMovieById
Scenario: Get Movie By Id
	Given I am a client
	When I Make GET Request '/movies/1/'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Rudy","yearOfRelease":1993,"plot":"Rudy has always been told that he was too small to play college football.","producerId":2,"coverUrl":"url","movieActors":[{"id":2,"name":"Terrence Howard"}],"movieProducers":[{"id":2,"name":"Robert N. Fried"}],"movieGenres":[{"id":5,"name":"Sports"},{"id":6,"name":"Drama"}]}]'

@PostMovie
Scenario: Add Movie
	Given I am a client
	When data items are '{"name":"Dangal","yearOfRelease":2017,"plot":"Sports movie ","producerId":2,"coverUrl":"url","actors":["1","2"],"genres":["3","5"]}'
	And I Make POST Request '/movies/'
	Then response code must be '200'


@PutMovie
Scenario: Update Movie By Id
	Given I am a client
	When data items are '{"name":"Dangal","yearOfRelease":2017,"plot":"Sports movie ","producerId":2,"coverUrl":"url","actors":["1","2"],"genres":["3","5"]}'
	And I Make PUT Request '/movies/1/'
	Then response code must be '200'


@DeleteMovieById
Scenario: Delete Movie By Id
	Given I am a client
	When I Make DELETE Request '/movies/1'
	Then response code must be '200'

@PostInvalidMovie
Scenario: Add Invalid Movie
	Given I am a client
	When data items are '{"name":"Dangal","yearOfRelease":2050,"plot":"Sports movie ","producerId":2,"coverUrl":"url","actors":["1","2"],"genres":["3","5"]}'
	And I Make POST Request '/movies/'
	Then response code must be '400'

@DeleteUnavailableActorById
Scenario: Delete Unavailable Movie By Id
	Given I am a client
	When I Make DELETE Request '/movies/-1'
	Then response code must be '404'
	And response data must look like '"Movie with Id -1 Not Available"'


@GetUnavailableMovieById
Scenario: Get Unavailable Movie By Id
	Given I am a client
	When I Make GET Request '/movies/-1'
	Then response code must be '404'
	And response data must look like '"Movie with Id -1 Not Available"'