﻿Feature: Genre Test

@getAllGenres
Scenario: Get All Genre
	Given I am a client
	When I Make GET Request '/genres'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Action"},{"id":2,"name":"War"},{"id":3,"name":"Superhero"},{"id":4,"name":"Science Fiction"},{"id":5,"name":"Sports"},{"id":6,"name":"Drama"}]'

@getGenreById
Scenario: Get Genre By Id
	Given I am a client
	When I Make GET Request '/genres/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Action"}]'

@PostGenre
Scenario: Add Genre
	Given I am a client
	When data items are '{"name":"Adventure"}'
	And I Make POST Request '/genres/'
	Then response code must be '200'


@PutGenre
Scenario: Update Genre By Id
	Given I am a client
	When data items are '{"name":"Romance"}'
	And I Make PUT Request '/genres/1'
	Then response code must be '200'


@DeleteGenreById
Scenario: Delete Genre By Id
	Given I am a client
	When I Make DELETE Request '/genres/1'
	Then response code must be '200'

	

@PostInvalidGenre
Scenario: Add Invalid Genre
	Given I am a client
	When data items are '{"name":""}'
	And I Make POST Request '/genres/'
	Then response code must be '400'

@DeleteUnavailableGenreById
Scenario: Delete Unavailable Genre By Id
	Given I am a client
	When I Make DELETE Request '/genres/-1'
	Then response code must be '404'
	And response data must look like '"Genre with Id -1 Not Available"'


@GetUnavailableGenreById
Scenario: Get Unavailable Genre By Id
	Given I am a client
	When I Make GET Request '/genres/-1'
	Then response code must be '404'
	And response data must look like '"Genre with Id -1 Not Available"'

