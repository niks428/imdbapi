CREATE DATABASE IMDBAPI

USE IMDBAPI

CREATE TABLE Actors (
	Id INT IDENTITY(1, 1) PRIMARY KEY
	,Name VARCHAR(40)
	,Bio VARCHAR(MAX)
	,DOB DATE
	,Gender VARCHAR(20)
	);

CREATE TABLE Producers (
	Id INT IDENTITY(1, 1) PRIMARY KEY
	,Name VARCHAR(40)
	,Bio VARCHAR(MAX)
	,DOB DATE
	,Gender VARCHAR(20)
	);

CREATE TABLE Genres (
	Id INT IDENTITY(1, 1) PRIMARY KEY
	,Name VARCHAR(80)
	);

CREATE TABLE Movies (
	Id INT IDENTITY(1, 1) PRIMARY KEY
	,Name VARCHAR(80)
	,YearOfRelease INT
	,Plot VARCHAR(MAX)
	,ProducerID INT FOREIGN KEY REFERENCES Producers(Id)
	,CoverUrl VARCHAR(MAX)
	);

CREATE TABLE MovieActorMapping (
	MovieId INT FOREIGN KEY REFERENCES Movies(Id)
	,ActorId INT FOREIGN KEY REFERENCES Actors(Id)
	)

CREATE TABLE MovieGenreMapping (
	MovieId INT FOREIGN KEY REFERENCES Movies(Id)
	,GenreId INT FOREIGN KEY REFERENCES Genres(Id)
	)

--If producerId not present in Producer table then roolback
 CREATE PROCEDURE usp_AddMovie @Name NVARCHAR(MAX)
	,@YearOfRelease INT
	,@Plot NVARCHAR(MAX)
	,@ProducerId INT
	,@CoverUrl NVARCHAR(MAX)
	,@ActorIds VARCHAR(MAX)
	,@GenreIds VARCHAR(MAX)
AS
BEGIN TRANSACTION

BEGIN TRY
	DECLARE @movieId INT;

	INSERT INTO Movies
	VALUES (
		@Name
		,@YearOfRelease
		,@Plot
		,@ProducerId
		,@CoverUrl
		);

	SET @movieId = @@IDENTITY;

	INSERT INTO MovieActorMapping
	SELECT @movieId [MovieId]
		,[value] [ActorId]
	FROM string_split(@ActorIds, ',')

	INSERT INTO MovieGenreMapping
	SELECT @movieId [MovieId]
		,[value] [GenreId]
	FROM string_split(@GenreIds, ',')

	COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
END CATCH


--EXEC usp_AddMovie @Name = 'IronMan'
--	,@YearOfRelease = 1992
--	,@Plot = 'Plot of mov 2'
--	,@ProducerId = 1
--	,@CoverUrl = 'url://'
--	,@ActorIds = '1,2'
--	,@GenreIds = '1,2'

CREATE PROCEDURE usp_UpdateMovie @Id INT
	,@Name NVARCHAR(MAX)
	,@YearOfRelease INT
	,@Plot NVARCHAR(MAX)
	,@ProducerId INT
	,@CoverUrl NVARCHAR(MAX)
	,@ActorIds VARCHAR(MAX)
	,@GenreIds VARCHAR(MAX)
AS
BEGIN
	UPDATE Movies
	SET Name = @Name
		,YearOfRelease = @YearOfRelease
		,Plot = @Plot
		,ProducerId = @ProducerId
		,CoverUrl = @CoverUrl
	WHERE Id = @Id;

	DELETE
	FROM MovieActorMapping
	WHERE MovieId = @Id;

	INSERT INTO MovieActorMapping
	SELECT @Id [MovieId]
		,[value] [ActorId]
	FROM string_split(@ActorIds, ',')

	DELETE
	FROM MovieGenreMapping
	WHERE MovieId = @Id;

	INSERT INTO MovieGenreMapping
	SELECT @Id [MovieId]
		,[value] [GenreId]
	FROM string_split(@GenreIds, ',')
END

--EXEC usp_UpdateMovie @Id = 2
--	,@Name = 'Hulk'
--	,@YearOfRelease = 2005
--	,@Plot = 'Plot of mov 2'
--	,@ProducerId = 1
--	,@CoverUrl = 'url://'
--	,@ActorIds = '1'
--	,@GenreIds = '1,2';

CREATE PROCEDURE usp_DeleteMovie @Id INT
AS
BEGIN
	DELETE
	FROM MovieActorMapping
	WHERE MovieId = @Id;

	DELETE
	FROM MovieGenreMapping
	WHERE MovieId = @Id;

	DELETE
	FROM Movies
	WHERE Id = @Id;
END

--EXEC usp_DeleteMovie @Id = 2;

CREATE PROCEDURE usp_DeleteActor @Id INT
AS
BEGIN
	DELETE
	FROM MovieActorMapping
	WHERE ActorId = @Id;

	DELETE
	FROM Actors
	WHERE Id = @Id;
END

CREATE PROCEDURE usp_DeleteGenre @Id INT
AS
BEGIN
	DELETE
	FROM MovieGenreMapping
	WHERE GenreId = @Id;

	DELETE
	FROM Genres
	WHERE Id = @Id;
END

--Used 1.When we delete Producer we also delete movie and respectively movie from mapping tables(movieActor,movieGenre)
CREATE PROCEDURE usp_DeleteProducer @Id INT
AS
BEGIN
	DECLARE @MovieId INT;

	SELECT @MovieId=Id FROM Movies WHERE ProducerID=@Id;

	DELETE
	FROM MovieActorMapping
	WHERE MovieId = @MovieId;

	DELETE
	FROM MovieGenreMapping
	WHERE MovieId = @MovieId;

	DELETE
	FROM Movies
	WHERE ProducerID = @Id;

	DELETE
	FROM Producers
	WHERE Id = @Id;
END

----Inactive/NotUsed 2. We set producer Id First producer Aailable else Null in movies table and safely delete producer from producer table while keeping left tables untouched
CREATE PROCEDURE usp_DeleteProducerA @Id INT
AS
BEGIN
	DECLARE @NewProducerID INT
	SET @NewProducerID = NULL
	IF 
		(
			SELECT Id
			FROM Producers
			WHERE Id NOT IN (@Id)
		) <> NULL
	BEGIN

		SET @NewProducerID = (
				SELECT TOP 1 Id
				FROM Producers
				WHERE Id NOT IN (@Id)
				);
	END

	UPDATE Movies
	SET ProducerID = @NewProducerID
	WHERE ProducerID = @ID;

	DELETE
	FROM Producers
	WHERE Id = @Id

END



